<a name="downloadthetheme"></a>
### Download the theme ###
Ready to get started? First you need to download your theme's zip file from bindtuning.com. Access your theme details page and click on the Download button to download your theme.

![gettingstarted_1](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/gettingstarted_1.gif) 



<a name="insidethe.zipfile"></a>
### Inside the theme package ###
Let's take a look at what the file contains. Inside your theme package you will find three folders, one for rels, another for Content and the other for package. Plus two more files.


- [Content_Types]
- Orchard.Theme.yourtheme.nuspec

- ***rels*** 

- ***Content**

	Inside the ***Content*** folder you will find everything related to your theme's content and styles. Including CSS stules, JavaScript files, PNG files, GIF files, CSS files, CSHTML files. 


- **package** 

	Inside the ***package*** folder you will find a .psmdcp file.
 



