BindTuning themes are packed with demo content files that are easy to set up and are a great jump start for building content areas for your website. 

**Important:** Your theme must be installed and applied to the site before uploading the demo content.

<a name="downloadthe.zipfile"></a>
### Download the .zip file 
Log in to your account at [bindtuning.com](http://bindtuning.com). Inside your account dashboard click on *My Downloads* and **select the theme**. 
Inside the theme's page, under documentation, click on the demo content link to download the .zip file.


<a name="unzipthefile"></a>
### Unzip the file

Before uploading the demo content into your website you will need to unzip the file. The file name is ***theme.ORX1.DemoContent.zip***.

Inside the .xml file you will find content for different zones, including the Navigation, footer, etc. Plus a folder with demo images. If you want to add a slider zone check our video on [Adding a slider in Orchard](http://support.bindtuning.com/hc/en-us/articles/204447519-Adding-a-slider-in-Orchard)).



<a name="uploadingthedemocontent"></a>
### Uploading the demo content 
Follow these steps to add the demo content into your website: 

1. **Go to your admin area** (http://yourURL/admin); 
2. You might need to **install *Import Export* Module**;
	1. Go to *Modules*;
	2. Search by *Import Export*;
	![uploadthedemocontent_2](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uploadthedemocontent_2.png) 
	3. Check and click *Enable*;
3. From the left menu **open the "Import Export"** module;
	![uploadthedemocontent_4](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uploadthedemocontent_4.png) 
4. On the "Import" tab **upload the demo content file.xml**. Select the correct xml file according to your installed Orchard version. If you have a version below 1.9 select "DemoContent_Before_v1.9.xml". If you have equal or above 1.9 select "DemoContent.xml". You can check on the footer of your admin dashboard the version of your Orchard installation.
	![uploadthedemocontent_5](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uploadthedemocontent_5.png) 
5. Now **click *Import***. All done! 
	![uploadthedemocontent_7](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uploadthedemocontent_7.png) 	
	
	

All demo pages will use the default layout, if you want to set a specific layout read [Set a specific layout](#fsdsd).

**Note:** The demo images are not automatically imported, they need to be uploaded manually on the Media Library and update on the demo content their url paths.

**Note:** The demo content will create a new menu, so you need to "Unpublish" the default menu "MainMenu" on the admin Widgets section to use the new menu.

**Note:** To set one of the demo pages as home page check the option "Set as home page" on the page edit options in Content section.



Looking good! On to the next chapter. 

