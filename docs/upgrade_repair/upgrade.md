**Warning:** ALWAYS make a backup of all of your theme assets, including CSS files, master pages, page layouts,... Upgrading the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading, clean your browser cache.

Upgrading your theme is as easy as preparing yourself a cup of coffee. The only thing you need to do is install the theme package again and that's it.

<a name="beforeupgrading"></a>
###Before upgrading 
Before upgrading your theme you will need to request the new version at BindTuning.com. You can find all the instructions you need to upgrade your theme's version [here](http://support.bindtuning.com/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-).

![upgrade_1](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/upgrade_1.png)


Download the new upgraded version from your account and save it with the same name as the theme you have installed. In our case is *MDIFinal*.ORX1.nupkg. 


<a name="upgradingyourtheme"></a>
###Upgrading your theme 
Now that you have downloaded the upgraded version of your theme the only thing left to do is to upgrade the theme you've installed on your website. Here we go: 

1. Open your Orchard admin area; 
2. On the left menu, **click on *Themes***; 
	![upgrade_2](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/upgrade_2.png)
3. Open the *Packages* folder and **click on *Install a theme from your computer***;
4. Click on *Choose File* and **search for your theme package**, *yourtheme*.ORX1.nupkg; 
	![upgrade_3](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/upgrade_3.png)
5. Now click on "Install";


And that's it! You should now be able to see on your theme's details the current version. 


